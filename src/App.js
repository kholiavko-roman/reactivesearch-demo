import React, { Component } from 'react';
import { ReactiveBase, DataSearch, CategorySearch } from '@appbaseio/reactivesearch';
import theme from './theme';
import './App.css';

import Results from './components/Results';
import Header from './components/Header';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTopics: [],
    };
  }




  render() {
    return (
      <section className="container">
        <ReactiveBase
          app="ads"
          credentials="elastic:elastic"
          url="http://localhost:9200"
          // type="gitxplore-latest"
          theme={theme}
        >
          <div className="flex row-reverse app-container">
            <Header />
            <div className="results-container">
              <CategorySearch
                componentId="name"
                dataField={['customFields.ro.title', 'customFields.ru.title']}
                categoryField="category.keyword"
                // autosuggest
                // placeholder="Search Repos"
                URLParams
                // className="data-search-container results-container"
                // innerClass={{
                //   input: 'search-input',
                // }}
                // defaultQuery={(value, props, category) => {
                //   console.log('defaultQuery', value);

                //   return {
                //     match: {
                //       'customFields.ro.title': '11',
                //       'customFields.ru.title': '11'
                //     }
                //   }
                // }}
                onValueSelected={
                  function(value, category, cause, source) {
                    console.log("current value and category: ", value, category)
                  }
                }
                renderSuggestions={({
                  currentValue,
                  categories,
                  isOpen,
                  getItemProps,
                  highlightedIndex,
                  suggestions,
                  parsedSuggestions,
                }) => isOpen && Boolean(currentValue.length) && (
                  <div style={{ position: 'absolute', background:'red', zIndex: 9999, padding: 10, color: '#424242', fontSize: '0.9rem', border: '1px solid #ddd', borderRadius: 4, marginTop: 10, width: '100%' }}>
                    {
                      <div>
                        {parsedSuggestions.slice(0, 5).map((suggestion, index) => (
                          <div style={{ padding: 10, background: index === highlightedIndex ? '#eee' : 'transparent' }} key={suggestion.value} {...getItemProps({ item: suggestion })}>{suggestion.value}</div>
                        ))}
                        {categories.slice(0, 3).map((category, index) => (
                          <div style={{ padding: 10, color: 'mediumseagreen', background: highlightedIndex === index + parsedSuggestions.slice(0, 5).length ? '#eee' : 'transparent' }} key={category.key} {...getItemProps({ item: { value: currentValue, category: category.key } })}>{currentValue} in {category.key}</div>
                        ))}
                        {Boolean(currentValue.length) && <div style={{ color: 'dodgerblue', padding: 10, cursor: 'pointer', background: highlightedIndex === parsedSuggestions.slice(0, 5).length + categories.slice(0, 3).length ? '#eee' : 'transparent' }} {...getItemProps({ item: { label: currentValue, value: currentValue }})}>Search for "{currentValue}" in all categories</div>}
                      </div>
                    }
                  </div>
                )}
                react={{
                  and: ['category'],
                }}
              />
              {/* <DataSearch
                componentId="name"
                filterLabel="Search"
                // dataField={['name', 'alternative_names']}
                dataField={['customFields.ro.title', 'customFields.ru.title']}
                placeholder="Search Repos"
                iconPosition="left"
                autosuggest
                URLParams
                className="data-search-container results-container"
                innerClass={{
                  input: 'search-input',
                }}
              /> */}
              <Results />
            </div>
          </div>
        </ReactiveBase>
      </section>
    );
  }
}

export default App;