import React, { Component } from 'react';
import { SingleDropdownList, SingleDataList } from '@appbaseio/reactivesearch';

class SearchFilters extends Component {
    constructor() {
        super();

        this.state = {
            category: ' ',
        }
    }

    render() {
        return(
            <div>as
                Search filters go here!
                <SingleDropdownList
                    componentId="subCat"
                    dataField="subCat.keyword"
                    title="SubCategory id"
                    react={{
                        and: ['name', 'category'],
                    }}
                    URLParams
                    //
                    // data={
                    //     [{
                    //       label: "Legcovie",
                    //       value: "5a7605124fab0f0740d81fb0"
                    //     }, {
                    //       label: "doma",
                    //       value: "5acfe0592311cf4690519986"
                    //     }]
                    //   }
                />
                <SingleDataList
                    componentId="category"
                    dataField="category.keyword"
                    title="Category id"
                    // URLParams
                    innerClass={{
                        list: 'list',
                    }}
                    react={{
                        and: ['name', 'subCat'],
                    }}
                    // value={this.state.category}
                    URLParams
                    // onChange={(event) => {
                    //     console.log(event.target);
                    //     this.setState({
                    //         category: event.target.value
                    //     });
                    // }}
                    data={
                        [{
                          label: "Cat1",
                          value: "5a7605024fab0f0740d81fad"
                        }, {
                          label: "Cat2",
                          value: "5a76050a4fab0f0740d81fae"
                        }]
                      }
                />

            </div>
        )
    }
}

export default SearchFilters;