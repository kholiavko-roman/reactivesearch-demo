import React from 'react';
import { SelectedFilters, ReactiveList, DataController } from '@appbaseio/reactivesearch';

const onResultStats = (results, time, ...rest) => (
  <div className="flex justify-end">
    {results} results found in {time}ms
    {rest}
  </div>
);

const onData = (data) => (
  <div className="result-item" key={data.translitName}>
    {data.name}
    {data.customFields.ru.title}
  </div>
);

const onAllData = (data, ...rest) => {
  console.log(data);
  console.log(rest);
  return null;
}

const Results = () => (
  <div className="result-list">
    <SelectedFilters
      className="m1"
      clearAllLabel="Clear allfilters"
    />

    {/* <DataController
      componentId="DataControllerSensor"
      title="Data Controller Component"
      dataField="name"
      showFilter={true}
      filterLabel="Venue filter"
      URLParams={false}
      customQuery={
        function(value, props) {
          console.log('customQuery ', value);
          // return {
          //   match: {
          //     data_field: "this is a test"
          //   }
          // }
        }
      }
      // beforeValueChange={
      //   function(value) {
      //     // called before the value is set
      //     // returns a promise
      //     console.log('beforeValueChangge ');
      //     console.log(value);
      //   }
      // }
      onValueChange={
        function(value) {
          console.log("current value: ", value)
          // set the state
          // use the value with other js code
        }
      }
      onQueryChange={
        function(prevQuery, nextQuery) {
          // use the query with other js code
          console.log('prevQuery', prevQuery);
          console.log('nextQuery', nextQuery);
        }
      }
    >
      <p>A custom 💪 UI component</p>
    </DataController> */}

    <ReactiveList
      componentId="results"
      dataField="name"
      // onAllData={onAllData}
      onData={onData}
      onResultStats={onResultStats}
      react={{
        and: ['name', 'category', 'subCat'],
      }}
      pagination
      URLParams
      setPageURL={() =>{
        console.log('setPageURL')
        return '/asdfas/'
      }}
      innerClass={{
        list: 'result-list-container',
        pagination: 'result-list-pagination',
        resultsInfo: 'result-list-info',
        poweredBy: 'powered-by',
      }}
      size={6}
    />
  </div>
);

export default Results;